# frozen_string_literal: true

module Kettle
  module Soup
    class Railtie < Rails::Railtie
      railtie_name :kettle_soup

      if Rails.env.test? || Rails.env.development?
        rake_tasks do
          Kettle::Soup.install_tasks
        end
      end
    end
  end
end
