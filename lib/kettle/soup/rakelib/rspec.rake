# frozen_string_literal: true

begin
  if Kettle::Soup.contexts?('testing')
    require "rspec/core/rake_task"

    RSpec::Core::RakeTask.new(:spec)
  end
rescue LoadError
  desc("spec task stub")
  task(:spec) do
    warn(<<~NOTE
      rspec isn't installed, or is disabled for #{RUBY_VERSION} in the current environment.
      You may have found a bug in kettle-soup.
    NOTE
        )
  end
end
desc "alias test task to spec"
task :test => :spec
