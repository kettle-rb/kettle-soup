# frozen_string_literal: true

begin
  if Kettle::Soup.contexts?('docs')
    require "yard"

    YARD::Rake::YardocTask.new do |t|
      # This should obviously be configurable.  kettle-soup may need to read a config file for this file list.
      #
      # The Ruby source files (and any extra documentation files separated by '-')
      # to process.
      # @example Task files assignment
      #   YARD::Rake::YardocTask.new do |t|
      #     t.files   = ['app/**/*.rb', 'lib/**/*.rb', '-', 'doc/FAQ.md', 'doc/Changes.md']
      #   end
      t.files = %w[lib/**/*.rake lib/**/*.rb]
    end
  end
rescue LoadError
  task(:yard) do
    warn(<<~NOTE
      yard isn't installed, or is disabled for #{RUBY_VERSION} in the current environment.
      You may have found a bug in kettle-soup.
    NOTE
        )
  end
end
