# frozen_string_literal: true

if Kettle::Soup.contexts?('core')
  task default: Kettle::Soup::DEFAULT_TASKS
end
