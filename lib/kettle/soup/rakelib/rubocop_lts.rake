# frozen_string_literal: true

begin
  if Kettle::Soup.contexts?('style')
    require "rubocop/lts"

    Rubocop::Lts.install_tasks
  end
rescue LoadError
  task(:rubocop_gradual) do
    warn(<<~NOTE
      rubocop-lts isn't installed, or is disabled for #{RUBY_VERSION} in the current environment.
      You may have found a bug in kettle-soup.
    NOTE
        )
  end
end
