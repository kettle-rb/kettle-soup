# frozen_string_literal: true

# external libs
require "version_gem"

# this gem
require_relative "soup/version"
# :nocov:
require_relative "soup/railtie" if defined?(Rails::Railtie)
# :nocov:

module Kettle
  # In the medical devices field SOUP means "Software Of Unknown Provenance"
  module Soup
    class Error < StandardError; end

    ALLOWED_CONTEXTS = %w[
      core
      coverage
      debug
      docs
      repl
      style
      testing
    ].freeze

    ACTIVE_CONTEXTS = ENV.fetch("KETTLE_SOUP", "").split(",").freeze
    CONTEXT_TASKS = {
      "docs" => "yard",
      "style" => "rubocop_gradual",
      "testing" => "rspec"
    }.freeze
    DEFAULT_TASKS = ACTIVE_CONTEXTS.map { |context| CONTEXT_TASKS[context] }.compact

    module_function

    # For introspection
    def contexts?(*names)
      names.all? { |context| ACTIVE_CONTEXTS.include?(context) }
    end

    # Integration with Rake
    def install_tasks
      load("kettle/soup/tasks.rb")
    end

    # Integration with Bundler as a plugin
    def register
      return if defined?(@registered) && @registered

      bad_contexts = ACTIVE_CONTEXTS.reject { |context| ALLOWED_CONTEXTS.include?(context) }

      if bad_contexts.any?
        raise Error,
              "Unknown contexts specified: #{bad_contexts.inspect}. contexts should be one of #{ALLOWED_CONTEXTS.inspect}"
      end

      @registered = true

      Bundler::Plugin.add_hook("before-install-all") do |dependencies|
        dsl = Bundler::Dsl.new
        ACTIVE_CONTEXTS.each do |context|
          dsl.eval_gemfile("#{__dir__}/soup/gemfiles/#{context}.gemfile")
        end
        dependencies.concat(dsl.dependencies)
      end
    end
  end
end

Kettle::Soup::Version.class_eval do
  extend VersionGem::Basic
end
