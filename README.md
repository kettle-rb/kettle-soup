<p align="center">
  <a href="https://kettle-rb.gitlab.io/" target="_blank" rel="noopener">
    <img height="120px" src="https://kettle-rb.gitlab.io/assets/img/logos/logo-name-optimized.png" alt="kettle-rb logo, Copyright (c) 2023 Peter Boling, CC BY-SA 4.0, see https://kettle-rb.gitlab.io/logos">
  </a>
</p>

# ⚠️⛔️☣️☢️ WARNING ⚠️⛔️☣️☢️
This is a non-working POC.
For discussion see: https://github.com/rubygems/rubygems/issues/6939

# 🍜 Kettle::Soup

This library provides Shared Gemfile contexts.

These can be useful for people tasked with maintenance of many libraries, 
and come in especially handy in building CI pipelines.

The name is derived from the medical devices field,
where this library is considered a package of [SOUP](https://en.wikipedia.org/wiki/Software_of_unknown_pedigree).

## ✨ Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add kettle-soup

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install kettle-soup

## 💎 Ruby Version Support

Minimum supported runtime Ruby version is 2.7, but can be used in projects that support older versions of Ruby,
while being developed in recent versions of Ruby, as this gem doesn't add runtime dependencies.

## 🔧 Basic Usage

In a Gemfile add:
```ruby
plugin "kettle-soup"
```

In your CI settings, shell profile, or on the command line, setup the following ENV variable:
```shell
export KETTLE_SOUP=core,coverage,debug,docs,repl,style,testing
bundle install
```
or as a single command:
```shell
KETTLE_SOUP=core,coverage,debug,docs,repl,style,testing bundle install
```

### 💰Funding 💰

*IMPORTANT* - Using this gem keeps literally dozens of gems from being listed in your gemfile.
When you run `bundle fund` you will _not see_ those gems as being part of your tool chain asking for funding.
You _do_ run `bundle fund`... right?  What to do?

```shell
KETTLE_SOUP=core,coverage,debug,docs,repl,style,testing bundle fund
```

Your friendly neighborhood spider&lt;ahem&gt; open source contributor thanks you!

### ⚙️ Customization

`KETTLE_SOUP` should be customized such that you only load the relevant gemfile contexts.
For example, in a CI pipeline where you run tests, you might want `KETTLE_SOUP=core,testing`,
but if you are also doing code coverage in the same pipeline, then `KETTLE_SOUP=core,coverage,testing`,
and if you have a separate pipeline for RuboCop linting, then `KETTLE_SOUP=core,style`.

The gemfile contexts you can choose from are:

- **core**
  - `rake`
- **coverage**
  - `simplecov`
  - `simplecov-cobertura` - XML output for XML-compatible tools (e.g. Jenkins)
  - `simplecov-json` - JSON output for JSON-compatible tools (e.g. CodeClimate)
  - `simplecov-lcov` - LCOV output for LCOV-compatible tools (e.g. GCOV)
  - `simplecov-rcov` - RCOV output for and RCOV-compatible tools (e.g. Hudson)
- **debug**
  - `debug` when platform in: [:mri, :mingw, :x64_mingw]
  - `byebug` when `ENV["KETTLE_DEBUGGER"] == 'byebug'`
  - `pry-debugger-jruby` when platform in: [:jruby]
  - and when `ENV["KETTLE_DEBUG_IDE"] == 'true'` and when platform in: [:mri, :mingw, :x64_mingw, :jruby]
    - `debase` 
    - `ruby-debug-ide`
- **docs**
  - `redcarpet`
  - `yard`
  - `yard-junk`
- **repl**
  - `pry`
  - `pry-coolline`
  - `pry-doc`
  - `pry-docmore`
  - `pry-highlight`
  - `pry-macro`
  - `pry-pretty-numeric`
  - `pry-rescue`
- **style**
  - `kettle-rb`
    - Note: Automatically installs latest version of `rubocop-ruby*_*` compatible with your ruby version.
      - I.e. with a little setup you can have RuboCop rules customized for the exact version of Ruby you have.
  - `rubocop-packaging`
  - `rubocop-rspec`
  - `standard-rubocop-lts`
- **testing**
  - `rspec`
  - `rspec-block_is_expected`
  - `rspec-stubbed_env`

## 💻 Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake spec` to run the tests w/ coverage,
or `bin/rake` to run tests w/ coverage, and linting.
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

### 🚀 Release Instructions

See [CONTRIBUTING.md][contributing].

## ⚡️ Contributing

See [CONTRIBUTING.md][contributing]

## 🌈 Contributors

See: [https://gitlab.com/kettle-rb/kettle-soup/-/graphs/main][🖐contributors]

## 📄 License

The gem is available as open source under the terms of
the [MIT License][license] [![License: MIT][license-img]][license-ref].
See [LICENSE.txt][license] for the official [Copyright Notice][copyright-notice-explainer].

<details>
  <summary>Organization Logos (kettle-rb)</summary>

See [kettle-rb.gitlab.io/logos][organization-logos]
</details>

[organization-logos]: https://kettle-rb.gitlab.io/logos

### © Copyright

* Copyright (c) 2023 [Peter H. Boling][peterboling] of [Rails Bling][railsbling]

## 🤝 Code of Conduct

Everyone interacting in this project's codebases, issue trackers, [chat rooms][🏘chat] and mailing lists is expected to follow the [code of conduct][🤝conduct].

## 📌 Versioning

This library aims to adhere to [Semantic Versioning 2.0.0][semver]. Violations of this scheme should be reported as
bugs. Specifically, if a minor or patch version is released that breaks backward compatibility, a new version should be
immediately released that restores compatibility. Breaking changes to the public API will only be introduced with new
major versions.

As a result of this policy, you can (and should) specify a dependency on this gem using
the [Pessimistic Version Constraint][pvc] with two digits of precision.

For example, in a `Gemfile`:

```ruby
plugin "kettle-soup", "~> 1.0"
```

[🤝conduct]: https://gitlab.com/kettle-rb/kettle-soup/-/blob/main/CODE_OF_CONDUCT.md
[🖐contributors]: https://gitlab.com/kettle-rb/kettle-soup/-/graphs/main
[🚎src-main]: https://gitlab.com/kettle-rb/kettle-soup/-/tree/main
[🏘chat]: https://app.gitter.im/#/room/#kettle-rb:gitter.im
[rubygems-security-guide]: https://guides.rubygems.org/security/#building-gems

[aboutme]: https://about.me/peter.boling
[angelme]: https://angel.co/peter-boling
[blogpage]: http://www.railsbling.com/tags/kettle-soup/
[codecov_coverage]: https://codecov.io/gh/kettle-rb/kettle-soup
[code_triage]: https://www.codetriage.com/kettle-rb/kettle-soup
[climate_coverage]: https://codeclimate.com/github/kettle-rb/kettle-soup/test_coverage
[climate_maintainability]: https://codeclimate.com/github/kettle-rb/kettle-soup/maintainability
[copyright-notice-explainer]: https://opensource.stackexchange.com/questions/5778/why-do-licenses-such-as-the-mit-license-specify-a-single-year
[contributing]: https://gitlab.com/kettle-rb/kettle-soup/-/blob/main/CONTRIBUTING.md
[devto]: https://dev.to/galtzo
[documentation]: https://rubydoc.info/github/kettle-rb/kettle-soup/main
[followme]: https://img.shields.io/twitter/follow/galtzo.svg?style=social&label=Follow
[gh_actions]: https://github.com/kettle-rb/kettle-soup/actions
[gh_discussions]: https://github.com/kettle-rb/kettle-soup/discussions
[gh_sponsors]: https://github.com/sponsors/pboling
[issues]: https://gitlab.com/kettle-rb/kettle-soup/-/issues
[liberapay_donate]: https://liberapay.com/pboling/donate
[license]: LICENSE.txt
[license-ref]: https://opensource.org/licenses/MIT
[license-img]: https://img.shields.io/badge/License-MIT-green.svg
[peterboling]: http://www.peterboling.com
[pvc]: http://guides.rubygems.org/patterns/#pessimistic-version-constraint
[railsbling]: http://www.railsbling.com
[rubygems]: https://rubygems.org/gems/kettle-soup
[security]: https://gitlab.com/kettle-rb/kettle-soup/-/blob/main/SECURITY.md
[semver]: http://semver.org/
[tweetme]: http://twitter.com/galtzo
